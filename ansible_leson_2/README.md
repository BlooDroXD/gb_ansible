# SECOND LESSON

## TREE OF LAB
```bash
├── ansible_jenkins.sh
├── install_jenkins.sh
├── inventories
│   └── dev
│       ├── group_vars
│       │   └── servers.yaml
│       └── hosts
├── pb_jenkins.yml
├── pb.yaml
├── README.md
├── roles
│   ├── create_grp_and_usr
│   │   ├── defaults
│   │   │   └── main.yml
│   │   └── tasks
│   │       └── main.yml
│   ├── ensure_file
│   │   ├── defaults
│   │   │   └── main.yml
│   │   └── tasks
│   │       └── main.yaml
│   └── install_jenkins
│       ├── default
│       └── tasks
│           └── main.yml
└── run_bp.sh


```

