#!/bin/bash

ansible -i inventories/dev/ servers \
	-k -K -vv -m copy -a "src=install_jenkins.sh dest=/tmp mode=0777"

ansible -i inventories/dev/ servers \
        -k -K -vv -m command -a "sh /tmp/install_jenkins.sh" -b

ansible -i inventories/dev/ servers \
        -k -K -vv -m command \
       	-a "cat /var/lib/jenkins/secrets/initialAdminPassword" -b

ansible -i inventories/dev/ servers \
        -k -K -vv -m file \
        -a "/tmp/install_jenkins.sh state=absent" -b





